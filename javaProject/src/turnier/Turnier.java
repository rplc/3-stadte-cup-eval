package turnier;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Turnier {
	private String ausrichter;
	private Date datum;
	private ArrayList<Result> results = new ArrayList<Result>();
	SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
	
	public Turnier(Element xMLElement) {
		ausrichter = xMLElement.getAttribute("ausrichter");
		try {
			datum = sdf.parse(xMLElement.getAttribute("datum"));
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		
		NodeList nList = xMLElement.getElementsByTagName("verein");
		for (int i = 0; i < nList.getLength(); i++) {
			Node nNode = nList.item(i);
			if (nNode.getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nNode;
				
				String name = e.getAttribute("name");
				Integer ergebnis = Integer.parseInt(e.getTextContent());
				
				results.add(new Result(name, ergebnis));
			}
		}
	}
	
	public String getAusrichter() {
		return ausrichter;
	}
	
	public String getDate() {
		return sdf.format(datum);
	}
	
	public ArrayList<Result> getResults() {
		return results;
	}
	
	@Override
	public String toString() {
		String s = "Ausrichter: " + ausrichter + ", Datum: " + sdf.format(datum) + "\n";
		for(Result res: results) {
			s += res.getVerein() + " \t " + res.getErgebnis() + "\n";
		}
		s += "\n";
		
		return s;
	}

}
