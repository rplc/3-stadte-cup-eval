package turnier;

public class Result {
	private String verein;
	private Integer ergebnis;
	
	public Result(String verein, Integer ergebnis) {
		this.verein = verein;
		this.ergebnis = ergebnis;
	}
	
	public String getVerein() {
		return verein;
	}
	
	public Integer getErgebnis() {
		return ergebnis;
	}
}
