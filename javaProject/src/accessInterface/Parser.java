package accessInterface;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import net.ucanaccess.jdbc.UcanaccessDriver;

@SuppressWarnings("unused")
public class Parser {
	
	Connection con = null;
	public void establishConnection(File file) throws SQLException {
		Connection con = DriverManager.getConnection("jdbc:ucanaccess://" + file.getAbsolutePath() + ";memory=true");

		this.con = con;
	}
	
	public void dumpData() throws SQLException {
		Statement st = con.createStatement();

		ResultSet rs = st.executeQuery("SELECT * FROM `Teilnehmer THS`");

		while (rs.next()) {
			System.out.println(rs.getString("Vorname"));
			System.out.println(rs.getString("Nachname"));
		}
	}
	
}
