package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.sql.SQLException;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;

import accessInterface.Parser;
import net.miginfocom.swing.MigLayout;
import turnier.Result;
import turnier.Turnier;

public class MainPanel extends JPanel implements ActionListener {

	private static final long serialVersionUID = -971942787028414354L;
	private final JFileChooser fc = new JFileChooser();

	public MainPanel() {
		super(new MigLayout("wrap 4"));

		/*
		 * JButton openButton = new JButton("Datenbank öffnen");
		 * openButton.setActionCommand("filechooser.open");
		 * openButton.addActionListener(this); this.add(openButton);
		 */

		ArrayList<Turnier> turniere = xmlInterface.Parser.getTurniere();

		this.add(new JLabel(""));
		this.add(new JLabel("Ausrichter"), "span 3");

		this.add(new JLabel(""));
		int idx = 0;

		LinkedList<String> vereine = new LinkedList<String>();
		for (Result r : turniere.get(0).getResults()) {
			vereine.add(r.getVerein());
			//TODO use Verein Class
		}
		Collections.sort(vereine, new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				return Collator.getInstance().compare(o1, o2);
			}
		});
		

		idx = 3;
		for (String verein: vereine) {
			this.add(new JLabel(verein), "cell 0 " + idx);
			idx++;
		}

		for (int i = 0; i < turniere.size(); i++) {
			idx = 1;

			Turnier t = turniere.get(i);

			this.add(new JLabel(t.getAusrichter()), "cell " + (i + 1) + " " + idx);

			idx++;
			this.add(new JLabel(t.getDate()), "cell " + (i + 1) + " " + idx);

			for (int y = 0; y < t.getResults().size(); y++) {
				Result r = t.getResults().get(y);

				Integer row = vereine.indexOf(r.getVerein()) + 3;
				
				this.add(new JLabel(r.getErgebnis() + ""), "cell " + (i + 1) + " " + row);
			}
		}
		
		//TODO loop vereine and add total result

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand() == "filechooser.open") {
			System.out.println("Action");

			fc.addChoosableFileFilter(new MDBFilter());
			fc.setAcceptAllFileFilterUsed(false);

			int returnVal = fc.showOpenDialog(this);

			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File file = fc.getSelectedFile();

				Parser p = new Parser();
				try {
					p.establishConnection(file);
					p.dumpData();
				} catch (SQLException e1) {
					// TODO Fehler beim Parsen

					e1.printStackTrace();
				}
			}
		}

	}

}
