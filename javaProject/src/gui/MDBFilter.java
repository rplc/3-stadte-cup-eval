package gui;

import java.io.File;

import javax.swing.filechooser.FileFilter;

public class MDBFilter extends FileFilter {

	@Override
	public boolean accept(File f) {
		if (f.isDirectory() || f.getName().equals("daten42.mdb")) {
            return true;
        } else {
        	return false;
        }
	}

	@Override
	public String getDescription() {
		return "Nur Glatz Datenbanken (daten42.mdb) möglich.";
	}

}
