package main;


import java.awt.BorderLayout;

import javax.swing.JFrame;

import gui.MainPanel;
import xmlInterface.Parser;

public class Main {
	
	private static void createGui() {
		JFrame frame = new JFrame("3 Städte Cup Evaluation");
		frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		frame.add(new MainPanel(), BorderLayout.CENTER);

		// Display the window.
		frame.pack();
		frame.setVisible(true);
	}

	public static void main(String[] args) {
		Parser.parseXML();
		
		createGui();
	}
}
