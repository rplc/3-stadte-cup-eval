package xmlInterface;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import turnier.Turnier;

public class Parser {

	private static ArrayList<Turnier> turniere = new ArrayList<Turnier>();

	public static void parseXML() {
		try {
			File inputFile = new File("data.xml");

			DocumentBuilder dBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();

			Document doc = dBuilder.parse(inputFile);
			doc.getDocumentElement().normalize();

			NodeList nList = doc.getElementsByTagName("turnier");

			for (int i = 0; i < nList.getLength(); i++) {
				Node nNode = nList.item(i);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					turniere.add(new Turnier((Element) nNode));
				}
			}
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}

	}
	
	public static ArrayList<Turnier> getTurniere() {
		return turniere;
	}

	public static void main(String[] args) {
		Parser.parseXML();

		for (Turnier t : Parser.turniere) {
			System.out.println(t);
		}
	}
}
