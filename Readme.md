# Anforderungen

- Verbindung mit der Access DB
  - File chooser für die daten42.mdb
- Config/XML mit teilnehmenden Vereinen, aktueller Ausrichter
  - teilnehmende Vereine evtl. gar nicht auflisten -> Liste aller Starter gruppiert nach Vereinen anzeigen und User muss dann auswählen?
- Datei-DB in der alle Läufer, Ergebnisse usw. der vergangenen 3 Städte Cups stehen
- Auslesen aller Starter des aktuellen 3 Städte Cups
- Auflistung aller Starter
  - Person hackt dann an, wer alles in der Mannschaft ist/in welcher Disziplin er gewertet wird
  - ggfs. Prüfung, dass keine Doppelstarts erlaubt sind
  - die Auflistung in einer Datei speichern (nur fürs spätere Debugging/Prüfen)
- evtl. Export als Bild/PDF


## JAVA
- https://stackoverflow.com/questions/21955256/#21955257
- Alternativ den Microsoft Access Driver mit ausliefern zum Installieren


## In MS Office
- Access
- Excel Macro